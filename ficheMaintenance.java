
import java.sql.*;
/**
 * 
 * @author Abiola AMOUUSA
 *
 */
public class ficheMaintenance {
	
	private int numMaintenance;
	private String typeMaintenance;
	private Date dateIntervention;
	private String auteur;

	public ficheMaintenance(){
		numMaintenance=0;
		dateIntervention=null;
		typeMaintenance=null;
		auteur=null;
		}
	
	public ficheMaintenance(String auteur, String typeMaintenance, int numMaintenance, Date dateIntervention) {
		this.numMaintenance = numMaintenance;
		this.dateIntervention= dateIntervention;
		this.typeMaintenance = typeMaintenance;
		this.auteur = auteur;
		}
	
/**
 * @return numMaintenance
 */
	public int getNumMaintenance() {
		return numMaintenance;
	}
	/**
	 * @return dateIntervention
	 */
	public Date getDateIntervention() {
		return dateIntervention;
	}
	/**
	 *@return auteur
	 */
	public String getAuteur() {
		return auteur;
	}
	/**
	 * @return typeMaintenance
	 */
	public String getTypeMaintenance() {
		return typeMaintenance;
	}
	/**
	 * @param typeMaintenance the tyupeMaintenance to set
	 */
	public void setTypeMaintenance(String typeMaintenance) {
		this.typeMaintenance = typeMaintenance;
	}
	/**
	 * @param auteur the auteur to set
	 */
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	/**
	 * @param dateIntervention the dateIntervention to set
	 */
	public void setDateIntervention(Date dateIntervention) {
		this.dateIntervention = dateIntervention;
	}
	/**
	 * @param numMaintenance the numIntervention to set
	 */
	public void setNumMaintenance(int numMaintenance) {
		this.numMaintenance = numMaintenance;
	}
}