CREATE TABLE client_clt (
    clt_id              VARCHAR2(20 BYTE) NOT NULL,
    clt_mdp             VARCHAR2(20 BYTE) NOT NULL,
    clt_nomentreprise   VARCHAR2(20 BYTE),
    clt_numerosiret     VARCHAR2(20 BYTE),
    clt_adresse         VARCHAR2(50 BYTE),
    clt_codeape         VARCHAR2(20 BYTE),
    CONSTRAINT client_clt_pk PRIMARY KEY ( clt_id )
        USING INDEX (
            CREATE UNIQUE INDEX client_clt_pk ON
                client_clt ( clt_id ASC )
                    LOGGING TABLESPACE system PCTFREE 10 INITRANS 2
                        STORAGE ( INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS UNLIMITED FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT )
                        NOPARALLEL
        )
    ENABLE
)
    LOGGING TABLESPACE system PCTFREE 10 PCTUSED 40 INITRANS 1
        STORAGE ( INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS UNLIMITED FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT )
    NOPARALLEL;