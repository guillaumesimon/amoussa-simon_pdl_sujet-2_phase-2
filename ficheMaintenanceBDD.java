import java.sql.*;
import java.sql.Date;
import java.util.*;
/**
 * 
 */

/**
 * 
 * @author Abiola
 *
 */
public class ficheMaintenanceBDD { 

		/**
		 * Param�tres de connexion � la base de donn�es oracle URL, LOGIN et PASS
		 * sont des constantes
		 */
		final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
		final static String LOGIN = "****"; 
		final static String PASS = "****";   

		
		/**
		 * Constructeur de la classe
		 * 
		 */
		public ficheMaintenanceBDD() {
			// chargement du pilote de bases de donn�ees
			try {
				Class.forName("oracle.jdbc.OracleDriver");
			} catch (ClassNotFoundException e) {
				System.err
						.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
			}

		}

		/**
		 * Permet d'ajouter une maintenance dans la table ficheMaintenance Le mode est auto-commit
		 * par d�faut : chaque insertion est valid�e
		 * 
		 * @param fichemaintenance
		 *            la maintenance � ajouter
		 * @return retourne le nombre de lignes ajout�es dans la table
		 */
		public int ajouter(ficheMaintenance ficheMaintenance) {
			Connection con = null;
			PreparedStatement ps = null;
			int retour = 0;

			// connexion � la base de donn�es
			try {

				// tentative de connexion
				con = DriverManager.getConnection(URL, LOGIN, PASS);
				// pr�paration de l'instruction SQL, chaque ? repr�sente une valeur
				// � communiquer dans l'insertion
				// les getters permettent de r�cup�rer les valeurs des attributs
				// souhait�s
				ps = con.prepareStatement("INSERT INTO ficheMaintenance (numMaintenance, typeMaintenance,dateIntervention,auteur)");
				ps.setInt(1,ficheMaintenance.getNumMaintenance());
				ps.setString(2,ficheMaintenance.getTypeMaintenance());
				ps.setString(3,ficheMaintenance.getAuteur());
				ps.setDate(4,ficheMaintenance.getDateIntervention());

				// Ex�cution de la requ�te
				retour = ps.executeUpdate();

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				// fermeture du preparedStatement et de la connexion
				try {
					if (ps != null)
						ps.close();
				} catch (Exception ignore) {
				}
				try {
					if (con != null)
						con.close();
				} catch (Exception ignore) {
				}
			}
			return retour;

		}

		/**
		 * Permet de r�cup�rer une maintenance�� partir de son identifiant
		 * 
		 * @param numSiret
		 *            le num�ro SIRET  du maintenance � r�cup�rer
		 * @return 	le maintenance trouv�;
		 * 			null si aucun client ne correspond � cette r�f�rence
		 */
		public ficheMaintenanceBDD getFicheMaintenance(String numSiret) {

			Connection con = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			ficheMaintenanceBDD retour = null;

			// connexion � la base de donn�es
			try {

				con = DriverManager.getConnection(URL, LOGIN, PASS);
				ps = con.prepareStatement("SELECT * FROM maintenance WHERE numSiret = ?");
				ps.setString(1, numSiret);

				// on ex�cute la requ�te
				// rs contient un pointeur situ� juste avant la premi�re ligne
				// retourn�e
				rs = ps.executeQuery();
				// passe � la premi�re (et unique) ligne retourn�e
				if (rs.next())
					retour = new ficheMaintenanceBDD();

			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				// fermeture du ResultSet, du PreparedStatement et de la Connexion
				try {
					if (rs != null)
						rs.close();
				} catch (Exception ignore) {
				}
				try {
					if (ps != null)
						ps.close();
				} catch (Exception ignore) {
				}
				try {
					if (con != null)
						con.close();
				} catch (Exception ignore) {
				}
			}
			return retour;

		}

		/**
		 * Permet de r�cup�rer tous les maintenances stock�s dans la table ficheMaintenances
		 * 
		 * @return une ArrayList de ficheMaintenance
		 */
		public static ArrayList<ficheMaintenanceBDD> getListeMaintenance() {

			Connection con = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			ArrayList<ficheMaintenanceBDD> retour = new ArrayList<ficheMaintenanceBDD>();

			// connexion � la base de donn�es
			try {

				con = DriverManager.getConnection(URL, LOGIN, PASS);
				ps = con.prepareStatement("SELECT * FROM ficheMaintenance");

				// on ex�cute la requ�te
				rs = ps.executeQuery();
				// on parcourt les lignes du r�sultat
				while (rs.next())
					retour.addAll(extracted(rs));

			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				// fermeture du rs, du preparedStatement et de la connexion
				try {
					if (rs != null)
						rs.close();
				} catch (Exception ignore) {
				}
				try {
					if (ps != null)
						ps.close();
				} catch (Exception ignore) {
				}
				try {
					if (con != null)
						con.close();
				} catch (Exception ignore) {
				}
			}
			return retour;

		}

		private static Collection<? extends ficheMaintenanceBDD> extracted(ResultSet rs) throws SQLException {
			return (Collection<? extends ficheMaintenanceBDD>) new ficheMaintenance (rs.getString("typeMaintenance"), rs.getString("auteur"),
				rs.getInt("numMaintenance"), rs.getDate("dateIntervention"));
		}

		// main permettant de tester la classe
		public static void main(String[] args) throws SQLException {

			ficheMaintenanceBDD maintenanceBDD = new ficheMaintenanceBDD();
			// test de la m�thode ajouter
			ficheMaintenance m1 = new ficheMaintenance();
			int retour = maintenanceBDD.ajouter(m1);

			System.out.println(retour + " lignes ajout�ees");

			// test de la m�thode getMaintenance
			ficheMaintenanceBDD m2 = ficheMaintenanceBDD.getFichMaintenanceBDD("0001");
			System.out.println(m2);

			// test de la m�thode getListeMaintenance
			ArrayList<ficheMaintenanceBDD> liste = ficheMaintenanceBDD.getListeMaintenance();
			// affichage des maintenances
			for (ficheMaintenanceBDD ficheMaintenanceBDD : liste) {
				System.out.println(ficheMaintenanceBDD.toString());
			}

		}

		private static ficheMaintenanceBDD getFichMaintenanceBDD(String string) {
			// TODO Auto-generated method stub
			return null;
		}
	}
