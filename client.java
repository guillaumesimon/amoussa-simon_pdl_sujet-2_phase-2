/**
 * Classe Article
 * @author Abiola AMOUSSA
 * @version 1.2
 * */

public class Client{

	
	/**
	 * nom de l'Entreprise
	 */
	private String nomEntrp;
	/**
	 * Numero de siret de l'entreprise du client
	 */
	private String numSiret;
	/**
	 * Code APE de l'entreprise du client
	 */
	private String codeAPE;
	/**
	 * Adresse
	 */
	private String adrEntrp;

	/**
	 * Constructeur
	 * @param nomEntrp 
	 * @param adrEntrp 
	 * @param numSiret
	 * @param codeAPE 
	 */
	public Client(String codeAPE, String numSiret, String adrEntrp, String nomEntrp) {
		this.nomEntrp = nomEntrp;
		this.adrEntrp = adrEntrp;
		this.numSiret = numSiret;
		this.codeAPE = codeAPE;
		
	}
	
	/**
	 * setter pour l'attribut adrEntrp
	 * @return adrEntrp
	 */
	public String getAdrEntrp() {
		return adrEntrp;
	}
	/**
	 * getter de l'attribut nomEntrp
	 * @return nomEntrp
	 */
	public String getNomEntrp() {
		return nomEntrp;
}
/**
 * getter  de l'attribut codeAPE
 * @return
 */
	public String getCodeAPE() {
	
		return codeAPE;
	}

	/**
	 * getter pour l'attribut numSiret
	 * @return
	 */
	public String getNumSiret() {
		
		return numSiret;
	}

	
	/**
	 * getter pour l'attribut adresse
	 * @return l'adresse
	 */

	/**
	 * Red�finition de la m�thode toString permettant de d�finir la traduction de la reference du client en String
	 * pour l'affichage
	 */
	public String toString() {
		return "Client [r�f : " + nomEntrp + " - " + adrEntrp
				+ ","+ codeAPE +","+ numSiret+"]";
	}

	
}

