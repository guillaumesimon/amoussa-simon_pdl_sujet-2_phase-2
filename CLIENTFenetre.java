import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

/**
 * Classe ClientFenetre
 * D�finit et ouvre une fenetre qui :
 * 
 *    - Permet l'insertion d'un nouvel client dans la table client grace
 * � la saisie du numSiret, CodeAPE,NomEntreprise, du departe et de adresse
 *    - Permet l'affichage de tous les clients
 * @author Abiola AMOUSSA
 * 
 * */


public class CLIENTFenetre extends JFrame implements ActionListener {
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class Client does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * Client
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour le champ numSiret
	 */
	private JTextField textFieldNumSiret;

	/**
	 * zone de texte pour le champ codeAPE
	 */
	private JTextField textFieldDeparte;
	/**
	 * Zone de texte pour le departement
	 */
	
	private JTextField textFieldCodeAPE;

	/**
	 * zone de texte pour le nom de l'entreprise du client
	 * 
	 */
	private JTextField textFieldNomEntrp;
	/**
	 * zone de texte pour l'adresse de l'entreprise
	 */
	private JTextField textFieldAdrEntrp;

	/**
	 * label numSiret
	 */
	private JLabel labelNumSiret;

	/**
	 * label codeAPE
	 */
	private JLabel labelCodeAPE;

	/**
	 * label le nom de l'entreprise
	 */
	
	private JLabel labelNomDeparte;
	/**
	 *label le departement 
	 */
	
	private JLabel labelNomEntrp;

	/**
	 * label adresse
	 */
	private JLabel labelAdrEntrp;

	/**
	 * bouton d'ajout de client
	 */
	private JButton boutonAjouter;

	/**
	 * bouton qui permet d'afficher tous les clients
	 */
	private JButton boutonAffichageDesClients;

	/**
	 * Zone de texte pour afficher les clients
	 */
	JTextArea zoneTextListClients;

	/**
	 * Zone de d�filement pour la zone de texte
	 */
	JScrollPane zoneDefilement;

	/**
	 * instance de ClientBDD permettant les acc�s � la base de donn�es
	 */
	private ClientBDD monClientBDD;

	/**
	 * Constructeur D�finit la fen�tre et ses composants - affiche la fen�tre
	 * @return 
	 */
	public void Client() {
		// on instancie la classe Article BDD
		this.monClientBDD=new ClientBDD();

		// on fixe le titre de la fen�tre
		this.setTitle(" Client");
		// initialisation de la taille de la fen�tre
		this.setSize(400, 400);

		// cr�ation du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de g�rer la position des �l�ments
		// il autorisera un retaillage de la fen�tre en conservant la
		// pr�sentation
		// BoxLayout permet de positionner les �lements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.GREEN);

		// instantiation des composants graphiques
		textFieldNumSiret = new JTextField();
		textFieldCodeAPE = new JTextField();
		textFieldAdrEntrp = new JTextField();
        textFieldNomEntrp = new JTextField();
        textFieldDeparte = new JTextField();
		boutonAjouter = new JButton("ajouter");
		boutonAffichageDesClients = new JButton(
				"affichage des  clients");
		labelNumSiret = new JLabel("NumSIRET :");
		labelCodeAPE = new JLabel("CodeAPE:");
		labelNomEntrp = new JLabel("AdrEntrp:");
		labelNomEntrp = new JLabel("NomEntrp:");
		labelNomDeparte = new JLabel("NomDeparte:");
		Component textFieldNomDeparte = null;
	

		JTextArea zoneTextListClient = new JTextArea(10, 20);
		zoneDefilement = new JScrollPane(zoneTextListClient);
		zoneTextListClient.setEditable(false);

		// ajout des composants sur le container
		containerPanel.add(labelNumSiret);
		// introduire un espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldNumSiret);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelCodeAPE);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldCodeAPE);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelNomEntrp);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldNomEntrp);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		containerPanel.add(labelNomDeparte);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldNomDeparte);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelAdrEntrp);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldAdrEntrp);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(boutonAjouter);

		containerPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		containerPanel.add(boutonAffichageDesClients);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(zoneDefilement);

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory
				.createEmptyBorder(10, 10, 10, 10));

		// ajout des �couteurs sur les boutons pour g�rer les �v�nements
		boutonAjouter.addActionListener(this);
		boutonAffichageDesClients.addActionListener(this);

		// permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fen�tre
		this.setVisible(true);
	}

	/**
	 * G�re les actions r�alis�es sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe ClientBDD

		try {
			if (ae.getSource() == boutonAjouter) {
				// on cr�e l'objet message
				 Client c = new Client(
						 	this.textFieldNumSiret.getText(),
							this.textFieldCodeAPE.getText(),
							this.textFieldNomEntrp.getText(),
							this.textFieldAdrEntrp.getText());
				// on demande � la classe de communication d'envoyer le client
				// dans la table client
				retour = monClientBDD.ajouter(c);
				// affichage du nombre de lignes ajout�es
				// dans la bdd pour v�rification
				System.out.println("" + retour + " ligne ajout�e ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, " client ajout� !");
				else
					JOptionPane.showMessageDialog(this, "erreur ajout client",
							"Erreur", JOptionPane.ERROR_MESSAGE);
			} else if (ae.getSource() == boutonAffichageDesClients) {
				// on demande � la classe ClientDAO d'ajouter le message
				// dans la base de donn�es
				ArrayList<Client> liste = monClientBDD.getListeClient();
				// on efface l'ancien contenu de la zone de texte
				zoneTextListClients.setText("");
				// on affiche dans la console du client les clients re�us
				for (Client a : liste) {
					zoneTextListClients.append(a.toString());
					zoneTextListClients.append("\n");
					// Pour afficher dans la console :
					// System.out.println(a.toString());
				}
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,
					"Controler la saisie", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Controler la saisie");
		}

	}

	public static void main(String[] args) {
		new CLIENTFenetre();
	}

}
