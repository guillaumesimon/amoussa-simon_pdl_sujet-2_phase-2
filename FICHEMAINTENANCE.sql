CREATE TABLE fichemaintenance (
    fm_idfiche            VARCHAR2(20) NOT NULL,
    fm_type_maintenance   VARCHAR2(50),
    fm_client             VARCHAR2(20),
    fm_operateur          VARCHAR2(20),
    fm_date               DATE,
    fm_etat               VARCHAR2(10),
    CONSTRAINT fichemaintenance_pk PRIMARY KEY ( fm_idfiche )
        USING INDEX (
            CREATE UNIQUE INDEX fichemaintenance_pk ON
                fichemaintenance ( fcm_idfiche ASC )
                    LOGGING TABLESPACE system PCTFREE 10 INITRANS 2
                        STORAGE ( INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS UNLIMITED FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT )
                        NOPARALLEL
        )
    ENABLE
)
    LOGGING TABLESPACE system PCTFREE 10 PCTUSED 40 INITRANS 1
        STORAGE ( INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS UNLIMITED FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT )
    NOPARALLEL;