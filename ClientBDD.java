	import java.sql.*;
	import java.util.ArrayList;
	import java.util.List;
	/**
	 * Classe d'acc�s aux donn�es de la table Client
	 * @author Abiola AMOUSSA
	 *
	 */

	public class ClientBDD {

	/**
	* Param�tres de connexion �  la base de donn�es oracle URL, LOGIN et
	*PASS
	* sont des constantes
	*/

	final static String URL ="jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "****";
	final static String PASS = "****";

	/**
	* Constructeur de la classe
	*
	*/

	public ClientBDD() {
		// chargement du pilote de bases de donn�es
				try {
					Class.forName("oracle.jdbc.OracleDriver");
				} catch (ClassNotFoundException e) {
					System.err
						.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
				}

			}

			/**
			 * Permet d'ajouter un client dans la table client Le mode est auto-commit
			 * par d�faut : chaque insertion est valid�e
			 * 
			 * @param client
			 *    le client � ajouter
			 * @return retourne le nombre de lignes ajout�es dans la table
			 */
			public int ajouter(Client client) {
				Connection con = null;
				PreparedStatement ps = null;
				int retour = 0;

				// connexion � la base de donn�es
				try {

					// tentative de connexion
					con = DriverManager.getConnection(URL, LOGIN, PASS);
					// pr�paration de l'instruction SQL, chaque ? repr�sente une valeur
					// � communiquer dans l'insertion
					// les getters permettent de r�cup�rer les valeurs des attributs
					// souhait�s
					ps = con.prepareStatement("INSERT INTO client (nomEntrep,adreEntrp, numSiret,codeAPRE");
					ps.setString(1, client.getNumSiret());
					ps.setString(2, client.getCodeAPE());
					ps.setString(3, client.getNomEntrp());
					ps.setString(4, client.getAdrEntrp());
			
					// Ex�cution de la requ�te
					retour = ps.executeUpdate();

				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// fermeture du preparedStatement et de la connexion
					try {
						if (ps != null)
							ps.close();
					} catch (Exception ignore) {
					}
					try {
						if (con != null)
							con.close();
					} catch (Exception ignore) {
					}
				}
				return retour;

			}

			/**
			 * Permet de r�cup�rer un client � partir de sa r�f�rence
			 * 
			 * @param 
			 *            la r�f�rence du client � r�cup�rer
			 * @return 	le client trouv�;
			 * 			null si aucun client ne correspond � cette r�f�rence
			 */
			public Client getClient(String numSiret ) {

				Connection con = null;
				PreparedStatement ps = null;
				ResultSet rs = null;
				Client retour = null;

				// connexion � la base de donn�es
				try {

					con = DriverManager.getConnection(URL, LOGIN, PASS);
					ps = con.prepareStatement("SELECT * FROM client WHERE nomEntrp");
					ps.setString(1,numSiret);

					// on ex�cute la requ�te
					// rs contient un pointeur situ� juste avant la premi�re ligne
					// retourn�e
					rs = ps.executeQuery();
					// passe � la premi�re (et unique) ligne retourn�e
					if (rs.next())
						retour = new Client(
								rs.getString("numSiret"),
								rs.getString("codeApe"),
								rs.getString("nomEntrp"),
								rs.getString("adrEntrp"));

				} catch (Exception ee) {
					ee.printStackTrace();
				} finally {
					// fermeture du ResultSet, du PreparedStatement et de la Connexion
					try {
						if (rs != null)
							rs.close();
					} catch (Exception ignore) {
					}
					try {
						if (ps != null)
							ps.close();
					} catch (Exception ignore) {
					}
					try {
						if (con != null)
							con.close();
					} catch (Exception ignore) {
					}
				}
				return retour;

			}

			/**
			 * Permet de r�cup�rer tous les clients stock�s dans la table article
			 * 
			 * @return une ArrayList d'un Client
			 */
			public ArrayList<Client> getListeClient() {

				Connection con = null;
				PreparedStatement ps = null;
				ResultSet rs = null;
				ArrayList<Client> retour = new ArrayList<Client>();

				// connexion � la base de donn�es
				try {

					con = DriverManager.getConnection(URL, LOGIN, PASS);
					ps = con.prepareStatement("SELECT * FROM client");

					// on ex�cute la requ�te
					rs = ps.executeQuery();
					// on parcourt les lignes du r�sultat
					while (rs.next())
						retour.add(new Client (rs.getString("numSiret"), 
								rs.getString("codeApe"),
								rs.getString("nomEntrp"),
						        rs.getString("adress")));

				} catch (Exception ee) {
					ee.printStackTrace();
				} finally {
					// fermeture du rs, du preparedStatement et de la connexion
					try {
						if (rs != null)
							rs.close();
					} catch (Exception ignore) {
					}
					try {
						if (ps != null)
							ps.close();
					} catch (Exception ignore) {
					}
					try {
						if (con != null)
							con.close();
					} catch (Exception ignore) {
					}
				}
				
	                 return retour;
			}

			// main permettant de tester la classe
			public static void main(String[] args) throws SQLException {

				ClientBDD clientBDD = new ClientBDD();
				// test de la m�thode ajouter
			Client c1= new Client("0001","0001","client1","adrEntrp1"); 
						
				int retour = clientBDD.ajouter(c1);

				System.out.println(retour + " lignes ajout�es");

				// test de la m�thode getClients
				Client c2 = clientBDD.getClient("0001");
				System.out.println(c2);

				// test de la m�thode getListeClients
				ArrayList<Client> liste = clientBDD.getListeClient();
				// affichage des articles
				for (Client client : liste) {
					System.out.println(client.toString());
				}

			}
		
	}
